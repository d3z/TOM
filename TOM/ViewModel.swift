//
//  ViewModel.swift
//  TOM
//
//  Created by Gareth Fleming on 17/02/2023.
//

import Foundation
import RegexBuilder

class ViewModel: ObservableObject {
    @Published var registers = [String: Int]()
    @Published var zx = 0
    
    @Published var log = ""
    private var lineNumber = 0
    
    func reset() {
        registers = [
            "ax": 0,
            "bx": 0,
            "cx": 0,
            "dx": 0,
            "ex": 0,
            "fx": 0,
            "gx": 0,
            "hx": 0
        ]
        zx = 0
        log = "Resetting all registers to their defaults..."
        lineNumber = 0
    }
    
    func runCodeString(code: String) {
        reset()
        guard code.isEmpty == false else { return }
        
        let movRegex = Regex { "mov "; matchDigits(); ", "; matchRegister() }
        let addRegex = Regex { "add "; matchRegister(); ", "; matchRegister() }
        let subRegex = Regex { "sub "; matchRegister(); ", "; matchRegister() }
        let copyRegex = Regex { "copy "; matchRegister(); ", "; matchRegister() }
        let andRegex = Regex { "and "; matchRegister(); ", "; matchRegister() }
        let orRegex = Regex { "or "; matchRegister(); ", "; matchRegister() }
        let cmpRegex = Regex { "cmp "; matchRegister(); ", "; matchRegister() }
        let jeqRegex = Regex { "jeq "; matchDigits() }
        let jnqRegex = Regex { "jnq "; matchDigits() }
        let jmpRegex = Regex { "jmp "; matchDigits() }
        
        let lines = code.trimmingCharacters(in: .whitespacesAndNewlines).components(separatedBy: .newlines)
        var commandsUsed = 0
        
        while lineNumber < lines.count {
            let line = lines[lineNumber]
            
            if let match = line.wholeMatch(of: movRegex) {
                mov(value: match.output.1, destination: match.output.2)
            } else if let match = line.wholeMatch(of: addRegex) {
                add(first: match.output.1, second: match.output.2)
            } else if let match = line.wholeMatch(of: subRegex) {
                sub(first: match.output.1, second: match.output.2)
            } else if let match = line.wholeMatch(of: copyRegex) {
                copy(source: match.output.1, destination: match.output.2)
            } else if let match = line.wholeMatch(of: andRegex) {
                and(first: match.output.1, second: match.output.2)
            } else if let match = line.wholeMatch(of: orRegex) {
                or(first: match.output.1, second: match.output.2)
            } else if let match = line.wholeMatch(of: cmpRegex) {
                cmp(first: match.output.1, second: match.output.2)
            } else if let match = line.wholeMatch(of: jmpRegex) {
                let destination = match.output.1
                guard destination < lines.count else {
                    addToLog("*** ERROR: Invalid line number \(destination) ***")
                    return
                }
                jumpTo(destinationLineNumber: destination)
            } else if let match = line.wholeMatch(of: jeqRegex) {
                jumpEqual(destinationLineNumber: match.output.1)
            } else if let match = line.wholeMatch(of: jnqRegex) {
                jumpNotEqual(destinationLineNumber: match.output.1)
            } else {
                addToLog("*** ERROR: Unknown command. Remember, all commands and registers are case-sensitive. ***")
                return
            }
            
            commandsUsed += 1
            
        guard commandsUsed < 10_000 else {
                addToLog("*** ERROR: Too many commands. Exiting. ***")
                return
            }
        }
    }
    
    private func matchDigits() -> TryCapture<(Substring, Int)> {
        TryCapture {
            OneOrMore(.digit)
        } transform: { number in
            Int(number)
        }
    }
    
    private func matchRegister() -> Capture<(Substring, String)> {
        Capture {
            "a"..."h"
            "x"
        } transform: { match in
            String(match)
        }
    }
    
    private func addToLog(_ message: String) {
        log += "\n Line \(lineNumber + 1): \(message)"
    }
    
    private func clamp(register: String) {
        if registers[register, default: 0] < 0 {
            addToLog("*** Warning: Line \(lineNumber + 1) has set \(register) to a value below 0. It has been clamped to 0. ***")
            registers[register] = 0
        } else if registers[register, default: 0] > 255 {
            addToLog("*** Warning: Line \(lineNumber + 1) has set \(register) to a value above 255. It has been clamped to 255. ***")
            registers[register] = 255
        }
    }
    
    private func mov(value: Int, destination: String) {
        registers[destination] = value
        addToLog("Moving \(value) into \(destination)")
        clamp(register: destination)
        lineNumber += 1
    }
    
    private func add(first: String, second: String) {
        registers[first, default:0] += registers[second, default:0]
        addToLog("Adding \(first) to \(second) and setting result in \(first)")
        clamp(register: first)
        lineNumber += 1
    }
    
    private func sub(first: String, second: String) {
        registers[first, default:0] -= registers[second, default:0]
        addToLog("Subtracting \(second) from \(first) and setting result in \(first)")
        clamp(register: first)
        lineNumber += 1
    }
    
    private func copy(source: String, destination: String) {
        registers[destination, default:0] = registers[source, default:0]
        addToLog("Copied value in \(source) to \(destination)")
        lineNumber += 1
    }
    
    private func and(first: String, second: String) {
        registers[first, default:0] &= registers[second, default:0]
        addToLog("ANDing \(first) and \(second)")
        lineNumber += 1
    }
    
    private func or(first: String, second: String) {
        registers[first, default:0] |= registers[second, default:0]
        addToLog("ORing \(first) and \(second)")
        lineNumber += 1
    }
    
    private func cmp(first: String, second: String) {
        addToLog("Comparing \(first) to \(second) (zx will contain the result)")
        if registers[first, default:0] == registers[second, default:0] {
            zx = 1
        } else {
            zx = 0
        }
        lineNumber += 1
    }


    private func jumpTo(destinationLineNumber: Int) {
        addToLog("Jumping to line \(destinationLineNumber)")
        lineNumber = destinationLineNumber - 1
    }
    
    private func jumpEqual(destinationLineNumber: Int) {
        addToLog("Jumping to \(destinationLineNumber) if zx is 1")
        if zx == 1 {
            jumpTo(destinationLineNumber: destinationLineNumber)
        } else {
            addToLog("Did not jump to \(destinationLineNumber)")
            lineNumber += 1
        }
    }
    
    private func jumpNotEqual(destinationLineNumber: Int) {
        addToLog("Jumping to \(destinationLineNumber) if zx is 0")
        if zx == 0 {
            jumpTo(destinationLineNumber: destinationLineNumber)
        } else {
            addToLog("Did not jump to \(destinationLineNumber)")
            lineNumber += 1
        }
    }
}
