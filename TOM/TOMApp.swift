//
//  TOMApp.swift
//  TOM
//
//  Created by Gareth Fleming on 17/02/2023.
//

import SwiftUI

@main
struct TOMApp: App {
    var body: some Scene {
        DocumentGroup(newDocument: TOMDocument()) { file in
            ContentView(document: file.$document)
        }
    }
}
